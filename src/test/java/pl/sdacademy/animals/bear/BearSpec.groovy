package pl.sdacademy.animals.bear

import spock.lang.Specification

class BearSpec extends Specification {

    def "Bear should not be alive immediately after creation"() {
        given:
        int weight = 3
        Bear bear = new BlackBear(weight)

        when:
        boolean  result = bear.isAlive()

        then:
        !result
    }

    def "Bear should be alive if it has eaten within 10 days"() {
        /*
            FILL ME IN
        */
    }

    def "Bear should not be alive if it has eaten within more than 10 days"() {

    }

}
